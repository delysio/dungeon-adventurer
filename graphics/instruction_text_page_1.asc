In distant lands lived a brave knight   
named Reynard de Lorraine, known for    
his fearlessness and willingness to     
serve justice. One day he was given a   
mission by the King of Palnor to        
traverse the mysterious dungeons that   
hid forgotten treasures in their        
depths. The people of the kingdom had   
high hopes for him, trusting in his     
ability to overcome all odds.           
                                        
" hourglass - resets time               
# sword - kills bats on contact         
$ shield - provides protection from bats
% map - allows you to see the dungeon   
        for a few seconds               
& entrance to the next level            
                                        
     PRESS ANY KEY FOR NEXT PAGE         
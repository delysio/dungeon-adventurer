MAP_LOCATION_X: array[0..MAX_LEVEL-1] of byte = (2, 9, 11, 9, 3, 13, 25, 37, 25, 7, 1, 13, 11, 1, 35, 3, 3, 7, 5, 37);
MAP_LOCATION_Y: array[0..MAX_LEVEL-1] of byte = (7, 11, 5, 15, 13, 3, 13, 1, 11, 9, 19, 11, 13, 15, 1, 15, 9, 15, 19, 1);
SHIELD_LOCATION_X: array[0..MAX_LEVEL-1] of byte = (5, 31, 19, 23, 37, 23, 9, 17, 9, 21, 19, 21, 13, 29, 7, 37, 5, 29, 11, 27);
SHIELD_LOCATION_Y: array[0..MAX_LEVEL-1] of byte = (11, 3, 7, 9, 5, 7, 15, 7, 15, 11, 11, 13, 19, 9, 15, 7, 17, 13, 11, 11);
SWORD_LOCATION_X: array[0..MAX_LEVEL-1] of byte = (19, 19, 33, 37, 14, 13, 15, 11, 23, 29, 5, 14, 31, 7, 3, 9, 31, 29, 36, 9);
SWORD_LOCATION_Y: array[0..MAX_LEVEL-1] of byte = (5, 7, 3, 3, 7, 13, 7, 15, 5, 1, 5, 3, 13, 13, 5, 3, 9, 5, 1, 7);
TIMER_LOCATION_X: array[0..MAX_LEVEL-1] of byte = (23, 31, 9, 9, 27, 31, 19, 1, 17, 33, 25, 31, 37, 19, 31, 25, 23, 15, 29, 1);
TIMER_LOCATION_Y: array[0..MAX_LEVEL-1] of byte = (11, 17, 13, 9, 19, 19, 11, 19, 19, 17, 5, 11, 1, 9, 13, 17, 17, 19, 17, 7);
